"""
    create_ext(ext::Union{Vector{String},String}, countries::Vector{String})

Create a DataFrame that maps each country to a set of extensions.

# Arguments
- `ext`: A string or a vector of strings representing the extensions.
- `countries`: A vector of strings where each string is a country name.

# Returns
- A DataFrame where each row corresponds to a country, and each column (except the first one) corresponds to an extension. 
  The entries are 1 if the country has the extension, and 0 otherwise. The first column is the country name.

# Example
```julia
create_ext(["ev", "h2"], ["AT", "BE", "BG"])
```
"""
function create_ext(dieter_data::DieterData)
    ext = dieter_data.sets["extensions"]
    countries = dieter_data.sets["countries"]
    extensions = ext
    if isa(extensions, Dict)
        ext = Dict{String,Dict{String,Int64}}()
        for c in countries
            ext[c] = Dict{String,Int64}()
            for e in keys(extensions)
                if c in extensions[e]
                    ext[c][e] = 1
                else
                    ext[c][e] = 0
                end
            end
            for e in setdiff(["h2","heat","ev"],keys(extensions))
                ext[c][e] = 0
            end
        end
    elseif isa(extensions, Vector)
        ext = Dict{String,Dict}()
        for c in countries
            ext[c] = Dict{String,Int}()
            for e in extensions
                ext[c][e] = 1
            end
            for e in setdiff(["ev","h2","heat"],extensions)
                ext[c][e] = 0
            end
        end
    end
    extensions = ext
    tmp = vcat(DataFrames.DataFrame.([extensions[c] for c in countries])...)
    tmp[!,:n] = countries
    select!(tmp, [:n, :ev, :h2 , :heat])
    extensions = tmp
    return extensions
end


#function set_folder_struct(ext::Union{String,Vector{String}},countries::Vector{String},proj_year::Int64,weather_year::Int64,abs_path::Union{Nothing,String}=nothing)
function set_folder_struct(dieter_data::DieterData;abs_path::Union{Nothing,String}=nothing)

    td = Dates.today()
    proj_year = dieter_data.scalars["proj_year"]
    weather_year = dieter_data.scalars["weather_year"]
    countries = dieter_data.sets["countries"]
    res = dieter_data.scalars["ts_resolution"] > 1 ? string(dieter_data.scalars["ts_resolution"],"hr") : ""
    folder_name = "output/"*string(td)*"_"*string(proj_year)*string(weather_year)*string(length(countries))*"_"*(res)
    ext = keys(dieter_data.sets["extensions"]) |> collect
    if isa(ext, String)
        ext = [ext]
    end
    if isnothing(abs_path)
        abs_path = pwd()
    end
    folder_path = joinpath(abs_path,folder_name)
    mkpath(folder_path)
    mkpath(joinpath(folder_path,"core"))
    for e in ext
        if !isdir(joinpath(folder_path,e))
            mkdir(joinpath(folder_path,e))
        end
    end
    return folder_path
end

"""
    import_inflation_data(path::String, curr_year::Int64)

Import inflation data from a CSV file and re-base the inflation to the currency year.

# Arguments
- `path`: A string representing the path to the CSV file containing the inflation data.
- `curr_year`: An integer representing the currency year.

# Returns
- A DataFrame containing the inflation data, with the inflation re-based to the current year. 
  The DataFrame has two columns: `year` and `cpi`. The `year` column contains the year of each data point, 
  and the `cpi` column contains the Consumer Price Index (CPI) for that year, re-based to the currency year.

# Example
```julia
import_inflation_data("path/to/inflation_data.csv", 2021)
```
"""
function import_inflation_data(dieter_data::DieterData)
    path = joinpath(pwd(),dieter_data.paths["inflation"])
    curr_year = dieter_data.scalars["currency_year"]
    inflation = CSV.read(path,DataFrame)
    select!(inflation, [:TIME_PERIOD, :OBS_VALUE])
    # Re-base inflation 
    rename!(inflation, :OBS_VALUE => :cpi, :TIME_PERIOD => :year)
    inflation[!,:cpi] = 100 .* inflation[!,:cpi] ./ filter(row->row[:year]==curr_year, inflation)[!,:cpi][1]
    return inflation
end

function import_fuel_data(dieter_data::DieterData)
    path = joinpath(pwd(),dieter_data.paths["fuel_data"])
    fuel_data = CSV.read(path,DataFrame)
    select!(fuel_data, [ :fuel, :unit, :year, :priceyear, :price])
    return fuel_data
end



function find_fuel(tech,fuel_map)
    if tech in keys(fuel_map)
        return fuel_map[tech]
    else
        return missing
    end
end


function create_nodes_df(dieter_data::DieterData)
    nodes = DataFrame(:n => dieter_data.sets["countries"])
    nodes[!,:ev_quant] .= 0.0
    leftjoin!(nodes,dieter_data.h2_demand,on= :n => :country)
    rename!(nodes,:hydrogen => :hydrogen_yr_demand)
    nodes[!,:hydrogen_yr_demand] .= nodes[!,:hydrogen_yr_demand] .* 1e6
    nodes[!,:interest] .= dieter_data.scalars["interest_rate"]
    nodes[!,:co2_price] .= select(filter(row->row.year==dieter_data.scalars["proj_year"] && row.fuel == "carbon",dieter_data.fuel_data),:price)[1,:price]
    nodes[!,:co2_cap_exog] .= -1
    nodes[!,:phi_min_res] .= dieter_data.scalars["min_res"]
    nodes[!,:h2_dispatch] .= 1/8760
    return nodes
end

function create_scalars_df(dieter_data::DieterData)
    scalars = DataFrame(
        :settings => ["infeas_cost", "infeas_ev_cost", "infeas_h2_cost"],
        :value => [dieter_data.scalars["infeas_cost"], dieter_data.scalars["infeas_cost"], dieter_data.scalars["infeas_cost"]]

    )
    return scalars
end


function write_dieter_data(dieter_data;write_ts::Bool=true)
    # Create folder_name
    folder_path = set_folder_struct(dieter_data)

    # write main files
    dieter_data.extensions |> CSV.write(joinpath(folder_path,"extensions.csv"))
    create_nodes_df(dieter_data) |> CSV.write(joinpath(folder_path,"nodes.csv"))
    create_scalars_df(dieter_data) |> CSV.write(joinpath(folder_path,"scalars.csv"))

    # write settings
    settings = Dict()
    settings["sets_dict"] = dieter_data.sets
    settings["scalars_dict"] = dieter_data.scalars
    settings["path_dict"] = dieter_data.paths
    open(joinpath(folder_path,"settings.json"),"w") do f
        JSON.print(f,settings)
    end

    # copy mapping files
    cp("manual_data/maps.json",joinpath(folder_path,"maps.json"),force=true)
    cp("manual_data/metadata.json",joinpath(folder_path,"metadata.json"),force=true)


    # write core files
    write_availability(dieter_data,folder_path)
    dieter_data.demand_ts |> CSV.write(joinpath(folder_path,"core","load.csv"))

    DataFrame(:h => unique(dieter_data.demand_ts[!,:h])) |> CSV.write(joinpath(folder_path,"core","h.csv"))
    DataFrame(:n => dieter_data.sets["countries"]) |> CSV.write(joinpath(folder_path,"core","n.csv"))
    DataFrame(:tech => dieter_data.sets["tech"]) |> CSV.write(joinpath(folder_path,"core","tech.csv"))
    DataFrame(:sto => dieter_data.sets["sto"]) |> CSV.write(joinpath(folder_path,"core","sto.csv"))
    DataFrame(:l => dieter_data.sets["l"]) |> CSV.write(joinpath(folder_path,"core","l.csv"))
    DataFrame(:inflow_sto => intersect(dieter_data.sets["sto"],["rsvr","phs_open"])) |> CSV.write(joinpath(folder_path,"core","inflow_sto.csv"))
    DataFrame(:con => intersect(dieter_data.sets["tech"],["CCGT","OCGT","nuc","hc","lig","oil","other"])) |> CSV.write(joinpath(folder_path,"core","cons.csv"))
    DataFrame(:ren => setdiff(dieter_data.sets["tech"],["CCGT","OCGT","nuc","hc","lig","oil","other"])) |> CSV.write(joinpath(folder_path,"core","ren.csv"))
    DataFrame(:nondis => intersect(dieter_data.sets["tech"],["ror","wind_on","wind_off","pv"])) |> CSV.write(joinpath(folder_path,"core","nondis.csv"))
    DataFrame(:dis => setdiff(dieter_data.sets["tech"],["ror","wind_on","wind_off","pv"] )) |> CSV.write(joinpath(folder_path,"core","dis.csv"))
    DataFrame(:stoch => intersect(union(dieter_data.sets["tech"],dieter_data.sets["sto"]),["pv","wind_on","wind_off","ror","rsvr","phs_open"])) |> CSV.write(joinpath(folder_path,"core","stoch.csv"))

    dieter_data.technologies |> CSV.write(joinpath(folder_path,"core","technologies.csv"))
    dieter_data.storages |> CSV.write(joinpath(folder_path,"core","storages.csv"))
    dieter_data.lines |> CSV.write(joinpath(folder_path,"core","lines.csv"))
    dieter_data.incidence |> CSV.write(joinpath(folder_path,"core","incidence.csv"))

    # write h2 files
    if sum(dieter_data.extensions[:,:h2]) > 0
        DataFrame(:elyh2 => dieter_data.sets["elyh2"]) |> CSV.write(joinpath(folder_path,"h2","elyh2.csv"))
        DataFrame(:stoh2 => dieter_data.sets["stoh2"]) |> CSV.write(joinpath(folder_path,"h2","stoh2.csv"))
        DataFrame(:reconh2 => dieter_data.sets["reconh2"]) |> CSV.write(joinpath(folder_path,"h2","reconh2.csv"))
        DataFrame(:arcsh2 => dieter_data.h2_network[!,:arcsh2]) |> CSV.write(joinpath(folder_path,"h2","arcsh2.csv"))
        DataFrame(:importsh2 => filter(x->contains(x,"Import"),dieter_data.h2_network[!,:arcsh2])) |> CSV.write(joinpath(folder_path,"h2","importsh2.csv"))
        DataFrame(:connectorsh2 => filter(x->!contains(x,"Import"),dieter_data.h2_network[!,:arcsh2])) |> CSV.write(joinpath(folder_path,"h2","connectorsh2.csv"))
        dieter_data.h2_ely |> CSV.write(joinpath(folder_path,"h2","table_ely_tech.csv"))
        dieter_data.h2_storages |> CSV.write(joinpath(folder_path,"h2","table_stoh2_tech.csv"))
        dieter_data.h2_reconversion |> CSV.write(joinpath(folder_path,"h2","table_recon_tech.csv"))
        dieter_data.h2_network |> CSV.write(joinpath(folder_path,"h2","h2_arcs.csv"))
        dieter_data.h2_incidence |> CSV.write(joinpath(folder_path,"h2","h2_incidence.csv"))
        dieter_data.h2_incidence_im |> CSV.write(joinpath(folder_path,"h2","h2_incidence_imp.csv"))
        dieter_data.h2_incidence_ex |> CSV.write(joinpath(folder_path,"h2","h2_incidence_ex.csv"))
        # Write dummy df for hourly h2 demand
        dummy = DataFrame(:n => dieter_data.sets["countries"])
        dummy[!,:h] .= "t0001"
        dummy[!,:value] .= 0.0
        dummy |> CSV.write(joinpath(folder_path,"h2","par_h2_hydrogen_hr_demand.csv"))

    end

    if sum(dieter_data.extensions[!,:heat]) > 0
        write_heat(dieter_data,folder_path)
        dieter_data.heat_tech |> CSV.write(joinpath(folder_path,"heat","heat_tech.csv"))
        dieter_data.heat_shares |> CSV.write(joinpath(folder_path,"heat","heat_share.csv"))
        DataFrame(:heat_tech => dieter_data.sets["heat_tech"]) |> CSV.write(joinpath(folder_path,"heat","techheat.csv"))
        #DataFrame(:heat_sink => dieter_data.sets["heat_sink"]) |> CSV.write(joinpath(folder_path,"heat","sinkheat.csv"))
        DataFrame(:heat_building => dieter_data.sets["heat_building"]) |> CSV.write(joinpath(folder_path,"heat","heat_building.csv"))
    end

    if write_ts 
        write_time_series(dieter_data,folder_path)
    end


end

function write_time_series(dieter_data::DieterData,folder_name::String)
   if !isdir(joinpath(folder_name,"ts"))
       mkdir(joinpath(folder_name,"ts"))
   end
   for (k,v) in dieter_data.weather_ts
        for (k2,v2) in v
            CSV.write(joinpath(folder_name,"ts","availability_$(replace(k,"/"=>"_"))_$(k2).csv"),v2)
        end
    end
    if sum(dieter_data.extensions[!,:heat]) > 0
        for (k,v) in dieter_data.heat_ts
            for (k2,v2) in v
                hd = select(v2,[:n,:h,:heat_demand])
                cop = select(v2,[:n,:h,:airSourceFloor,:airSourceRadiator,:airSourceWater])
                CSV.write(joinpath(folder_name,"ts","heat_demand_$(replace(k,"/"=>"_"))_$(k2).csv"),hd)
                CSV.write(joinpath(folder_name,"ts","cop_$(replace(k,"/"=>"_"))_$(k2).csv"),cop)
            end
        end
    end
end


function write_availability(dieter_data::DieterData,folder_name::String)
    weather_year = dieter_data.scalars["weather_year"]
    season = string(weather_year,"/",weather_year+1) 
    dict = dieter_data.weather_ts[season] 
    df = vcat(values(dict)...)
    sort!(df,[:n,:h])
    select(df,[:n,:h,:pv,:wind_on,:wind_off,:ror]) |> CSV.write(joinpath(folder_name,"core","availability.csv"))
    select(df,[:n,:h,:rsvr,:phs_open]) |> CSV.write(joinpath(folder_name,"core","sto_inflow.csv"))
           
end

function write_heat(dieter_data::DieterData,folder_name::String)
    wy = dieter_data.scalars["weather_year"]
    season = string(wy,"/",wy+1)
    df = sort(vcat(values(dieter_data.heat_ts[season])...),[:n,:h])
    hd = select(df,[:n,:h,:heat_demand])
    cop = select(df,[:n,:h,:airSourceFloor,:airSourceRadiator,:airSourceWater])
    CSV.write(joinpath(folder_name,"heat","heat_demand.csv"),hd)
    CSV.write(joinpath(folder_name,"heat","cop.csv"),cop)
end

function reduce_time_series!(dieter_data::DieterData,resolution::Int64)
    # Reduce demand time series
    df = dieter_data.demand_ts
    cols = setdiff(Symbol.(names(df)),[:n,:h])
    tmp = df[!,cols]
    tmp = DataFrame(cols .=> compute_ts_averages.(eachcol(tmp),resolution))
    tmp[!,:h] .= [string("t",lpad(i,4,"0")) for i in 1:size(tmp,1)]
    dieter_data.demand_ts = tmp

    # Reduce weather time series

    for (k,v) in dieter_data.weather_ts
        for (k2,v2) in v
            dieter_data.weather_ts[k][k2] = reduce_weather(dieter_data.weather_ts[k][k2],resolution)
        end
    end

    # Reduce heat time series
    if sum(dieter_data.extensions[!,:heat]) > 0
        for (k,v) in dieter_data.heat_ts
            for (k2,v2) in v
                dieter_data.heat_ts[k][k2] = select!(dieter_data.heat_ts[k][k2],Not([:season,:month_seas]))
                dieter_data.heat_ts[k][k2] = reduce_weather(dieter_data.heat_ts[k][k2],resolution)
            end
        end
    end

end


function compute_ts_averages(ts_data::Array,resolution::Int64)
    tmp = ts_data
    out = mean.(Iterators.partition(tmp,resolution))
    return out
end


function reduce_weather(df::DataFrame,resolution)

    sort!(df,[:n,:h])
    cols = setdiff(Symbol.(names(df)),[:n,:h])
    tmp = df[!,cols]
    tmp = DataFrame(cols .=> compute_ts_averages.(eachcol(tmp),resolution))
    n = unique(df[!,:n])
    h = unique(df[!,:h])
    get_number(x) = parse(Int64,replace(x,"t"=>""))
    h_new = string.("t",lpad.(unique(ceil.(Int64,get_number.(h)./4)),4,"0"))
    ids = rename!(DataFrame(reduce(vcat,collect(Iterators.product(n,h_new)))),[Symbol("1"),Symbol("2")] .=> [:n,:h])
    sort!(ids,[:n,:h])
    tmp[!,:n] = ids[!,:n]
    tmp[!,:h] = ids[!,:h]
    select!(tmp,vcat([:n,:h],cols))
    return tmp
end
