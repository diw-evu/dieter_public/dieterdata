"""
    compile_technologies(dieter_data::DieterData)

Compile technology data from various sources, adjust for inflation, and expand to countries.

# Arguments
- `dieter_data`: A `DieterData` object containing various data related to the DIETER model.

# Returns
- A DataFrame containing the compiled technology data. Each row corresponds to a technology in a specific country. 
  The DataFrame includes columns for the technology name, various parameters, the fuel used by the technology, 
  the price of the fuel, whether the technology is enabled, the availability of the technology, and the country.

# Example
```julia
compile_technologies(dieter_data)
```
"""
function compile_technologies(dieter_data::DieterData)
    # Relevant data sets
    fuel_data = dieter_data.fuel_data
    
    tech_data = dieter_data.tech_data
    inflation = dieter_data.inflation

    # Scalar inputs
    proj_year = dieter_data.scalars["proj_year"]
    eraa_capas_path = haskey(dieter_data.paths,"eraa_capas_path") ? dieter_data.paths["eraa_capas_path"] : nothing
    res_potentials_path = haskey(dieter_data.paths,"res_potentials") ? dieter_data.paths["res_potentials"] : nothing

    # Sets
    countries = dieter_data.sets["countries"]
    tech = dieter_data.sets["tech"]

    # Prepare fuel data
    fuel = filter(row -> row.year == proj_year,fuel_data)
    fuel = leftjoin(fuel, inflation, on=Symbol("priceyear") => :year,matchmissing=:equal)
    fuel[ismissing.(fuel[!,:cpi]),:cpi] .= 100
    fuel[!,:price] = fuel[!,:price] ./ (fuel[!,:cpi] ./100)
    fuel = fuel[!,[:fuel,:price]]

    # Extract relevant data from database and adjust for inflation
    technologies = filter(row -> row.technology in tech, tech_data)
    technologies = filter(row -> row.year == proj_year, technologies)
    leftjoin!(technologies, inflation, on=Symbol("currency year") => :year,matchmissing=:equal)
    technologies[ismissing.(technologies[!,:cpi]),:cpi] .= 100
    technologies[!,:value] = parse.(Float64,technologies[!,:value]) ./ (technologies[!,:cpi] ./100)
    select!(technologies,[:technology,:parameter,:value])
    technologies = unstack(technologies, :technology, :parameter, :value)

    # Map technology to fuel
    fuel_map = Dict{String,String}()
    fuel_map["lig"] = "lignite"
    fuel_map["hc"] = "coal"
    fuel_map["OCGT"] = "gas"
    fuel_map["CCGT"] = "gas"
    fuel_map["bio"] = "biomass"
    fuel_map["oil"] = "oil"
    fuel_map["nuc"] = "nuclear"

    # Add fuel prices to technologies
    technologies[!,:fuel] = map(row -> find_fuel(row.technology,fuel_map), eachrow(technologies))
    technologies = leftjoin(technologies,fuel, on=:fuel, makeunique=true, matchmissing=:equal)
    
    # Add missing values
    technologies[!,:gen_tech_enabled] .= 1
    technologies[ismissing.(technologies[!,:price]),:price] .= 0 
    technologies[ismissing.(technologies[!,:availability]),:availability] .= 1

    # Expand technologies to countries
    technologies = leftjoin(rename!(DataFrame(Iterators.product(countries,tech)),[:n,:tech]),technologies, on= :tech=>:technology, makeunique=true, matchmissing=:equal)


    # get eraa capacity bound data
    upper_bound_gas = isempty(dieter_data.scalars["upper_bound_gas"]) ? 1e5 : dieter_data.scalars["upper_bound_gas"]
    if !isnothing(eraa_capas_path)
        eraa_year = isempty(dieter_data.scalars["eraa_year"]) ? 2030 : dieter_data.scalars["eraa_year"]
        ocgt_share = isempty(dieter_data.scalars["ocgt_share"]) ? 1/3 : dieter_data.scalars["ocgt_share"]
        url = eraa_capas_path
        eraa_df = CSV.read(HTTP.get(url).body,DataFrame)
        tech_map = DataFrame(:eraa_tech => unique(eraa_df[!,:tech]),
            :tech => ["nuc","lig","hc","CCGT","oil","ror","rsvr","phs_open","phs_closed","phs_open","phs_closed","Li-Ion","Li-Ion","wind_on","wind_off","solar_thermal","pv","bio","other","bio","dsr"],
            :type => ["power","power","power","power","power","power","power_out","power_out","power_out","power_in","power_in","power_in","power_out","power","power","power","power","power","power","power","power"]     
        )
        rename!(eraa_df, :tech=>:eraa_tech)
        filter!(row -> row.scenario == "National Estimates $(eraa_year)",eraa_df)
        eraa_df =leftjoin(eraa_df,tech_map, on=:eraa_tech, makeunique=true, matchmissing=:equal)
        eraa_capas = combine(groupby(eraa_df,[:country,:tech,:type]),:cap => sum)
        rename!(eraa_capas, :cap_sum => :capacity, :country => :n)
        filter(x->x.tech == "CCGT",eraa_capas)
    else
        @warn "No ERAA capacity data provided. Using default values."
    end
    # get RES potential data
    if !isnothing(res_potentials_path)
        url = res_potentials_path
        @assert haskey(dieter_data.scalars,"potential_type") "Require potential_type scalar to be set"
        potential_type = dieter_data.scalars["potential_type"]
        zip_url = HTTP.get(url).body
        zip = ZipFile.Reader(IOBuffer(zip_url))
        a_file_in_zip = filter(x->x.name =="national/$(potential_type)-potential/capacities.csv",zip.files)[1]
        potentials = CSV.File(read(a_file_in_zip)) |> DataFrame
        rename!(potentials,:id=>:iso3)
        dt = search_wdi("countries","iso2c",r".")
        rename!(dt, :iso2c => :iso2, :iso3c => :iso3)
        select!(dt, [:iso3,:iso2])
        potentials = leftjoin(potentials,dt, on=:iso3)
        potentials[!,:iso2] = replace.(potentials[!,:iso2], "GB" => "UK")
        potentials[!,:pv] = potentials[!,:open_field_pv_mw] .+ potentials[!,:rooftop_pv_mw]
        select!(potentials,[:iso2,:pv,:onshore_wind_mw,:offshore_wind_mw])
        rename!(potentials, :iso2 => :n,:onshore_wind_mw => :wind_on, :offshore_wind_mw => :wind_off)
        potentials = DataFrames.stack(potentials, Not(:n), variable_name=:tech, value_name=:potential)
    end
    # merge potentials and ERAA21 capas with technologies
    bounds = leftjoin(rename!(DataFrame(Iterators.product(countries,tech)),[:n,:tech]),select(eraa_capas,Not(:type)), on=[:n,:tech], makeunique=true, matchmissing=:equal)
    ocgt = leftjoin(rename!(bounds[(bounds[!,:tech] .== "CCGT") ,[:n,:capacity]],:capacity => :ccgt),filter(x->x.tech == "OCGT",eraa_capas)[!,[:n,:capacity]],on=:n) 
    ocgt[!,:capacity] = ocgt[!,:ccgt] .* ocgt_share
    bounds[bounds[!,:tech] .== "OCGT",:capacity] = ocgt[!,:capacity] 
    bounds[bounds[!,:tech] .== "CCGT",:capacity] = (1-ocgt_share) .* bounds[bounds[!,:tech] .== "CCGT",:capacity]
    bounds = leftjoin(bounds,potentials, on=[:n,:tech], makeunique=true, matchmissing=:equal)
    bounds[!,:minimum_capacity] .= 0.
    bounds[!,:maximum_capacity] .= 0.
    for row in eachrow(bounds)
        if row.tech in ["pv","wind_on","wind_off"]
            row.minimum_capacity = row.capacity
            row.maximum_capacity = row.potential
        elseif row.tech in ["CCGT","OCGT"]
            row.minimum_capacity = row.capacity
            row.maximum_capacity = upper_bound_gas
        elseif row.tech in ["nuc","ror"]
            row.minimum_capacity = row.capacity
            row.maximum_capacity = row.capacity
        else
            row.minimum_capacity = 0
            row.maximum_capacity = row.capacity
        end
    end
    technologies =  leftjoin(technologies,select!(bounds, [:n,:tech,:minimum_capacity,:maximum_capacity]), on=[:n,:tech], makeunique=true, matchmissing=:equal)
    
    # Add maximum energy for biomass
    url_ember = "https://ember-climate.org/app/uploads/2022/07/yearly_full_release_long_format.csv"
    ember_data = CSV.read(HTTP.get(url_ember).body,DataFrame)
    ember_data = leftjoin(ember_data,dt, on=Symbol("Country code") =>:iso3, makeunique=true, matchmissing=:equal)
    ember_data = filter(row -> !ismissing(row.iso2), ember_data)
    ember_data = filter(row -> row.iso2 in countries, ember_data)
    generation = select(filter(row -> (row.Category == "Electricity generation") & (row.Variable == "Bioenergy") & (row.Unit == "TWh") & (row.Year == 2022), ember_data),[:iso2,:Value])
    rename!(generation, :Value => :max_energy, :iso2 => :n)
    generation[:,:max_energy] = generation[:,:max_energy] .* 1e6
    generation[:,:tech] .= "bio" 
    technologies = leftjoin(technologies,generation, on=[:n,:tech], makeunique=true, matchmissing=:equal)
    technologies[ismissing.(technologies[!,:max_energy]),:max_energy] .= -1

    @pipe technologies |> sort(_,[:n,:tech])

    return technologies

end

# Get tech set subsets
function get_tech_subsets(dieter_data::DieterData)
    tech = dieter_data.sets["tech"]
    tmp_dict = Dict(
        "res" => intersect(tech,["pv","wind_on","wind_off","ror","bio"]),
        "con" => intersect(tech,["CCGT","OCGT","nuc","lig","hc","oil"]),
        "nondis" => intersect(tech,[ "ror","pv","wind_on","wind_off"]),
        "dis" => intersect(tech,["CCGT","OCGT","nuc","lig","hc","oil","bio"]),
    )
    for (k,v) in tmp_dict
        dieter_data.sets[k] = v
    end

    return nothing

end