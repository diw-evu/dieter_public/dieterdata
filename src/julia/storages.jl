function compile_storages(dieter_data::DieterData)
    sto = dieter_data.sets["sto"]
    countries = dieter_data.sets["countries"]
    tech_data = dieter_data.tech_data
    inflation = dieter_data.inflation
    
    proj_year = dieter_data.scalars["proj_year"]
    eraa_year_sto = dieter_data.scalars["eraa_year_sto"]
    eraa_sto_path = dieter_data.paths["eraa_sto_path"]


    storages = filter(row -> row.technology in sto, tech_data)
    filter!(row -> row.year == proj_year,storages)
    leftjoin!(storages, inflation, on=Symbol("currency year") => :year,matchmissing=:equal)
    storages[ismissing.(storages[!,:cpi]),:cpi] .= 100
    storages[!,:value] = parse.(Float64,storages[!,:value]) ./ (storages[!,:cpi] ./100)
    select!(storages,[:technology,:parameter,:value])
    storages = unstack(storages, :technology, :parameter, :value)
    if :overnight_energy_component in names(storages)
        select!(storages,Not(:overnight_energy_component))
    end

    eraa_url2 = eraa_sto_path
    res = HTTP.get(eraa_url2).body
    zip = ZipFile.Reader(IOBuffer(res))
    a_file_in_zip = filter(x->x.name =="PECD_EERA2021_reservoir_pumping_$(eraa_year_sto)_country_table.csv",zip.files)[1]
    sto_capas = CSV.File(read(a_file_in_zip)) |> DataFrame
    sto_dict = Dict{String,String}("reservoir" => "rsvr","pumped_open" => "phs_open","pumped_closed" => "phs_closed")	
    sto_capas[!,:sto] = map(row -> sto_dict[row.technology], eachrow(sto_capas))
    sto_capas[!,:value] = abs.(sto_capas[!,:value])
    replace!(sto_capas[!,:variable], "gen_cap_MW"=>"power_out","pumping_cap_MW"=> "power_in","sto_GWh"=>"energy")
    sto_capas = unstack(select(sto_capas,[:country,:sto,:variable,:value], [:sto,:country], :variable, :value))
    sto_capas[!,:energy] = sto_capas[!,:energy] .* 1e3
    rename!(sto_capas,:country => :n)
    sto_capas_max = rename(sto_capas,:energy => :max_energy,:power_in => :max_power_in,:power_out => :max_power_out)
    sto_capas_min = rename(sto_capas,:energy => :min_energy,:power_in => :min_power_in,:power_out => :min_power_out)
    sto_capas = outerjoin(sto_capas_max,sto_capas_min, on=[:n,:sto], makeunique=true, matchmissing=:equal)

    # Add to stroages
    rename!(storages, :technology => :sto)
    storages = leftjoin(rename!(DataFrame(Iterators.product(countries,sto)),[:n,:sto]),storages, on= :sto=>:sto, makeunique=true, matchmissing=:equal)

    storages = leftjoin(storages,sto_capas, on=[:n,:sto], makeunique=true, matchmissing=:equal)
    for x in ["energy","power_in","power_out"]
        storages[(storages[!,:sto].=="Li-Ion"),Symbol("max_$(x)")] .= -1.
        storages[(storages[!,:sto].=="Li-Ion"),Symbol("min_$(x)")] .= 0.
    end
    storages[!,:sto_marginal_cost] = Statistics.middle.(storages[:,:variable_costs_in],storages[:,:variable_costs_out])
    storages[!,:sto_tech_enabled] .= 1 
    storages[!,:fixed_costs_power_in] .= 0
    storages[!,:fixed_costs_power_out] = storages[!,:fixed_costs_power]
    
    # Add etop max
    storages[!,:etop_max] .= -1
    storages[(storages[!,:sto].=="Li-Ion"),:etop_max] .= 1e3
    #storages[(storages[!,:sto].!="Li-Ion"),:etop_max] .= -1

    # Fill missing values with 0
    for x in ["energy","power_in","power_out"]
        storages[ismissing.(storages[!,Symbol("max_$(x)")]),Symbol("max_$(x)")] .= 0.
        storages[ismissing.(storages[!,Symbol("min_$(x)")]),Symbol("min_$(x)")] .= 0.    
    end

    return storages

end



# Get tech set subsets
function get_sto_subsets(dieter_data::DieterData)
    sto = dieter_data.sets["sto"]
    tmp_dict = Dict(
        "inflow_sto" => intersect(tech,["rsvr","phs_open"]),
        "stoch" => ["pv","wind_on","wind_off","ror","rsvr","phs_open"]
    )
    for (k,v) in tmp_dict
        dieter_data.sets[k] = v
    end

    return nothing

end