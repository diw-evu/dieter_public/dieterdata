
using DataFrames
import CSV
import Dates
import Random
using HTTP
using ZipFile
using WorldBankData
using XLSX
using Pipe
using Statistics
using Latexify
using Parquet2
using RollingFunctions
using JSON

mutable struct DieterData
    sets::Dict{String,Any}
    scalars:: Dict{String,Union{Int64,Float64,String}} 
    paths::Dict{String,String}
    inflation::DataFrame
    fuel_data::DataFrame
    tech_data::DataFrame
    extensions::DataFrame
    technologies::DataFrame
    incidence::DataFrame
    lines::DataFrame
    storages::DataFrame
    h2_ely::DataFrame
    # h2 extension
    h2_storages::DataFrame
    h2_reconversion::DataFrame
    h2_network::DataFrame
    h2_incidence::DataFrame
    h2_incidence_im::DataFrame
    h2_incidence_ex::DataFrame
    # Heat extension
    heat_tech::DataFrame
    heat_shares::DataFrame

    # Time series
    weather_ts::Dict
    heat_ts::Dict
    demand_ts::DataFrame
    h2_demand::DataFrame



    function DieterData(sets_dict::Dict{String,Any},scalars_dict::Dict{String,Union{Int64,Float64,String}},paths_dict::Dict{String,String};export_csv::Bool=false)
        
        min_sets = ["countries","extensions","tech","sto"]
        min_scalars = ["proj_year","weather_year","currency_year","min_res","infeas_cost"]
        required_paths = ["inflation","fuel_data","tech_data"]

        
        self = new()
        self.sets = sets_dict
        self.scalars = scalars_dict
        self.paths = paths_dict
        @assert min_sets ⊆ keys(self.sets) "sets_dict needs to contain the following keys: countries, extensions, tech, sto"
        @assert min_scalars ⊆ keys(self.scalars) "scalars_dict needs to contain the following keys: proj_year, weather_year, currency_year"
        @assert required_paths ⊆ keys(self.paths) "paths_dict needs to contain the following keys: inflation, fuel_data, tech_data"
        self.inflation = import_inflation_data(self)
        self.fuel_data = import_fuel_data(self)
        self.tech_data = CSV.read(self.paths["tech_data"],DataFrame)
        self.extensions = create_ext(self)
        self.technologies = compile_technologies(self)
        self.storages = compile_storages(self)
        
        compile_grid_data!(self)

        #####################################################
        # Time series
        #####################################################
        self.demand_ts = compile_demand(self) # Note that H2 demand is added automatically
        self.weather_ts = compile_weather(self)
        

        
        #####################################################
        # Extensions
        #####################################################
        ## Hydrogen
        if sum(self.extensions[:,:h2]) > 0
            self.h2_ely = compile_h2_electrolysis(self)
            self.h2_storages = compile_h2_storages(self)
            self.h2_reconversion = compile_h2_recon(self)
            compile_h2_networks!(self)
        end 
        #####################################################
        ## Heat
        if sum(self.extensions[:,:heat]) > 0
            self.heat_ts = compile_heat_data(self)
            self.heat_tech = create_heat_tech_simple(self)
            self.heat_shares = create_heat_shares_simple(self)
        end

        #####################################################
        ## EV
        if sum(self.extensions[:,:ev]) > 0
            ### Add functions to compile EV data here
        end

        #####################################################
        # time series reduction
        if self.scalars["ts_resolution"] > 1 
            reduce_time_series!(self,self.scalars["ts_resolution"])
        end
        
        if export_csv
            set_folder_struct(self)
        end

        return self
    end
    DieterData() = new()
end