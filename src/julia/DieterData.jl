module DieterData
using DataFrames
import CSV
import Dates
import Random
using HTTP
using ZipFile
using WorldBankData
using XLSX
using Pipe
using Statistics
using Latexify
using Parquet

include("struct.jl")
include("util.jl")
include("generators.jl")
include("grid.jl")

export DieterData





end