## Functions to populate H2 module with data
function prepare_h2_data(dieter_data::DieterData)
    proj_year = dieter_data.scalars["proj_year"]
    tech_data = dieter_data.tech_data
    inflation = dieter_data.inflation
    
    h2_data = filter(row->row.module in ["h2"], tech_data)
    h2_data[ismissing.(h2_data[:,:year]),:year] .= proj_year
    h2_data = filter(row -> row.year == proj_year, h2_data)
    leftjoin!(h2_data, inflation, on=Symbol("currency year") => :year,matchmissing=:equal)
    h2_data[ismissing.(h2_data[!,:cpi]),:cpi] .= 100
    h2_data[!,:value] = replace.(h2_data[!,:value], "," => "") 
    h2_data[!,:value] = parse.(Float64,h2_data[!,:value]) ./ (h2_data[!,:cpi] ./100)
    select!(h2_data,[:technology,:parameter,:value])

    return h2_data

end

function compile_h2_electrolysis(dieter_data::DieterData)
  h2_data = prepare_h2_data(dieter_data)
  elyh2 = dieter_data.sets["elyh2"]
  h2_countries = select(filter(row->row.h2 == 1,dieter_data.extensions),:n) |> Array
  ely = unstack(filter(row->row.technology in elyh2,h2_data),:technology,:parameter,:value)
    rename!(ely, setdiff(names(ely),["technology"]).=> "h2_".*setdiff(names(ely),["technology"]).*"_ely")

    fill_comp = filter(row->row.technology == "filling compressor",h2_data)
    fill_comp[!,:parameter] = "h2_".*fill_comp[!,:parameter].*"_comp"
    fill_comp[!,:technology] .= "PEM" #TO DO: Make this work for multiple technologies
    fill_comp = unstack(fill_comp,:technology,:parameter,:value)


    ely = leftjoin(ely,fill_comp, on=:technology, makeunique=true, matchmissing=:equal)
    rename!(ely, :technology => :elyh2)
    ely = leftjoin(DataFrame(Iterators.product(h2_countries,elyh2),[:n,:elyh2]),ely, on=[:elyh2], makeunique=true, matchmissing=:equal)
    ely[!,:h2_min_power_ely] .= 0.
    ely[!,:h2_max_power_ely] .= -1.0
    ely[!,:h2_ely_enabled].= 1

    #select!(ely,Not(:h2_variable_costs_ely))
    
    return ely
    
end


function compile_h2_storages(dieter_data::DieterData)

    h2_data = prepare_h2_data(dieter_data)
    
    # import storage potential
    sto_pot = CSV.read(dieter_data.paths["cavern_potential_path"],DataFrame)
    select!(sto_pot,[:n,:potential])
    sto_pot[!,:stoh2] .= "h2 cavern"
    # Get data on storage
    stoh2 = dieter_data.sets["stoh2"]
    h2_countries = select(filter(row->row.h2 == 1,dieter_data.extensions),:n) |> Array
    sto = unstack(filter(row->row.technology in stoh2,h2_data),:technology,:parameter,:value)
    select!(sto,Not([:overnight_costs_tank,:overnight_costs_install]))
    rename!(sto, setdiff(names(sto),["technology"]).=> "h2_sto_".*setdiff(names(sto),["technology"]))

    # Storage compression
    comp = filter(row->row.technology in ["cavern storage compressor","tank storage compressor"],h2_data)
    comp[!,:parameter] = "h2_sto_".*comp[!,:parameter].*"_comp"
    comp[!,:technology] = replace.(comp[!,:technology],"cavern storage compressor" => "cavern","tank storage compressor" => "tank storage")
    comp = unstack(comp,:technology,:parameter,:value)
    comp = coalesce.(comp, 0.0)

    sto = leftjoin(sto,comp, on=:technology, makeunique=true, matchmissing=:equal)
    rename!(sto, :technology => :stoh2)
    sto = leftjoin(DataFrame(Iterators.product(h2_countries,stoh2),[:n,:stoh2]),sto, on=[:stoh2], makeunique=true, matchmissing=:equal)
    sto[!,:h2_sto_enabled] .= 1

    # Add storage potential
    leftjoin!(sto,sto_pot,on=[:n,:stoh2],makeunique=true,matchmissing=:equal)
    replace!(sto[!,:potential],missing => -1.0)
    rename!(sto,:potential => :h2_max_energy_sto)
    sto[!,:h2_min_energy_sto] .= 0.0
    sto[!,:h2_level_min_sto] .= 0.0
    # Add availability
    sto[!,:h2_sto_availability] .= 1.0

    
    return sto
end

function compile_h2_networks!(dieter_data::DieterData)
    h2_data = prepare_h2_data(dieter_data)
    h2_countries = select(filter(row->row.h2 == 1,dieter_data.extensions),:n) |> Array |> vec
    h2_net_capas = filter(row->row.technology == "h2 network net transfer capacity - reference",h2_data)
    h2_net_capas[!,:from] = map(row -> row.parameter[1:2], eachrow(h2_net_capas))
    h2_net_capas[!,:to] = map(row -> row.parameter[(end-1):end], eachrow(h2_net_capas))
    h2_net_capas[!,:active] .= 0
    for row in eachrow(h2_net_capas)
        if row.from in vcat(h2_countries,["Im"]) && row.to in h2_countries
            row.active = 1
        end
    end

    filter!(row -> row.active == 1, h2_net_capas)

    arcsh2_set = DataFrame(:arcsh2 => h2_net_capas[!,:parameter])
    dieter_data.sets["arcsh2"] = string.(arcsh2_set[:,:arcsh2])
    dieter_data.h2_network = DataFrame(:arcsh2 => h2_net_capas[!,:parameter], :h2_max_flow => h2_net_capas[!,:value])
    
    # compute boosting compressor demands
    comp = filter(row->row.technology == "boosting compressor",h2_data)
    dist = filter(row->(row.technology == "h2 network pipeline length") & (row.parameter in arcsh2_set[!,:arcsh2]),h2_data)
    dist[!,:h2_flow_comp_demand] =  (dist[!,:value]./1000).*comp[!,:value][1]
    select!(dist,[:parameter,:h2_flow_comp_demand])
    rename!(dist, :parameter => :arcsh2)
    dieter_data.h2_network = leftjoin(dieter_data.h2_network,dist,on=:arcsh2,makeunique=true,matchmissing=:equal)
    replace!(dieter_data.h2_network[!,:h2_flow_comp_demand],missing => 0.0)

    # Add import prices
    leftjoin!(dieter_data.h2_network, select(filter(:technology => ==("import price"), h2_data),Not(:technology)), on=:arcsh2 => :parameter, makeunique=true, matchmissing=:equal)
    rename!(dieter_data.h2_network, :value => :import_price)
    replace!(dieter_data.h2_network[!,:import_price],missing => 0.0)

    # Compute incidence matrices
    h2_incidence = select(h2_net_capas, [:parameter,:from,:to])
    for n in h2_countries
        h2_incidence[!,n] .= 0
    end
    for row in eachrow(h2_incidence)
        if row.from == "Im"
            row[row.to] = -1
        else
            row[row.from] = 1
            row[row.to] = -1
        end
    end
    select!(h2_incidence, Not(:from,:to))
    rename!(h2_incidence, :parameter => :arcsh2)


    h2_incidence_im = copy(h2_incidence)
    h2_incidence_ex = copy(h2_incidence)
    for c in vec(h2_countries)
        h2_incidence_im[!,c] = min.(h2_incidence[!,c],0)
        h2_incidence_ex[!,c] = max.(h2_incidence[!,c],0)
    end

    dieter_data.h2_incidence = h2_incidence
    dieter_data.h2_incidence_im = h2_incidence_im
    dieter_data.h2_incidence_ex = h2_incidence_ex


end

function compile_h2_recon(dieter_data::DieterData)
    h2_data = prepare_h2_data(dieter_data)
    reconh2 = dieter_data.sets["reconh2"]
    h2_countries = select(filter(row->row.h2 == 1,dieter_data.extensions),:n) |> Array |> vec

    recon = filter(row->row.technology in reconh2,h2_data)
    recon = unstack(recon,:technology,:parameter,:value)
    rename!(recon, setdiff(names(recon),["technology"]).=> "h2_".*setdiff(names(recon),["technology"]).*"_recon")
    rename!(recon, :technology => :reconh2)

    recon = leftjoin(DataFrame(Iterators.product(h2_countries,reconh2),[:n,:reconh2]),recon, on=[:reconh2], makeunique=true, matchmissing=:equal)
    recon[!,:h2_min_power_recon] .= 0.
    recon[!,:h2_max_power_recon] .= -1.0
    recon[!,:h2_recon_enabled].= 1

    return recon
end
