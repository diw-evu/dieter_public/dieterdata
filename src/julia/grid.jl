function compile_grid_data!(dieter_data::DieterData)
    
    proj_year = dieter_data.scalars["proj_year"]

    countries= dieter_data.sets["countries"]
    
    grid_data = filter(row->row.technology == "grid net transfer capacity", dieter_data.tech_data)
    filter!(row->row.year == proj_year,grid_data)
    grid_data[!,:from] = map(row -> row.parameter[1:2], eachrow(grid_data))
    grid_data[!,:to] = map(row -> row.parameter[4:5], eachrow(grid_data))
    grid_data[!,:active] .= 0
    for row in eachrow(grid_data)
        if row.from in countries && row.to in countries
            row.active = 1
        end
    end
    grid_data = filter(row -> row.active == 1, grid_data)

    # line sets
    line_set = DataFrame(:l => grid_data[!,:parameter])
    dieter_data.sets["l"] = [string(row)[(end-5):end] for row in eachrow(line_set)]
    
    # Create lines dataframe
    dieter_data.lines = DataFrame(:l => grid_data[!,:parameter], :max_flow => grid_data[!,:value], :eta_ntc => 1.0)

    # Create incidence matrix
    incidence = select(grid_data,[:parameter,:from,:to])
    for n in countries
        incidence[!,n] .= 0
    end
    for row in eachrow(incidence)
        row[row.from] = 1
        row[row.to] = -1
    end
    select!(incidence, Not(:from,:to))
    rename!(incidence, :parameter => :l)
    dieter_data.incidence = incidence  

    return nothing


end