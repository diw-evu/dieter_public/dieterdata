function compile_demand(dieter_data::DieterData)
    
    demand_year = dieter_data.scalars["demand_year"]
    # Use historical load data from Open Power System Data
    url = "https://data.open-power-system-data.org/time_series/2020-10-06/time_series_60min_singleindex.csv"
    demand = CSV.read(HTTP.get(url).body,DataFrame)
    
    
    # Filter to relevant field and countries
    tmp = filter(x->(x[1:2] in vcat(dieter_data.sets["countries"],["GB"])),filter(x->contains(x,"load_actual"),names(demand))) # Filter to relevant countries and actual load
    filter!(x->length(x)==minimum(length.(tmp)) || contains(x,"GB_UKM"),tmp) # drop TSO specific demands for Germany
    demand = select(demand,vcat([:cet_cest_timestamp,:utc_timestamp],Symbol.(tmp))) # Select relevant columns
    f = x-> string(x)[1:2]
    rename!(demand, tmp .=> Symbol.(f.(tmp)))
    if "GB" in names(demand)
        rename!(demand, :GB => :UK)
    end
    # Convert to Datetime
    g = x->Dates.DateTime(x[1:19],"yyyy-mm-ddTHH:MM:SS")
    demand[!,:cet_cest_timestamp] = g.(demand[!,:cet_cest_timestamp])

    # Filter to relevant years
    demand = filter(r -> Dates.year(r.cet_cest_timestamp) == demand_year || Dates.year(r.cet_cest_timestamp) == demand_year + 1,demand)
    demand = filter(r -> r.cet_cest_timestamp >= Dates.DateTime(demand_year,7,1) && r.cet_cest_timestamp < Dates.DateTime(demand_year+1,7,1),demand)

    if nrow(demand) > 8760
        demand = filter(r -> !((Dates.month(r.cet_cest_timestamp) == 2)&(Dates.day(r.cet_cest_timestamp) == 29)),demand)
    end
    describe(demand)
    # Add hour count
    demand[!,:h] = string.("t",lpad.(string.(collect(1:nrow(demand))),4,"0"))

    # Fix missing values by taking previous day's demand of the same hour
    demand = stack(demand, dieter_data.sets["countries"], variable_name=:country, value_name=:demand)
    miss = eachrow(filter(x->ismissing(x.demand),demand))
    for r in miss
        r.demand = filter(x->x.country == r.country && x.cet_cest_timestamp == r.cet_cest_timestamp - Dates.Day(1),demand)[!,:demand][1]

    end
    miss = DataFrame(miss)  
    demand[ismissing.(demand.demand),:demand] = miss[!,:demand]


    # Electrified heat demand correction
    if dieter_data.scalars["elec_heat_correction"] == 1
        w2h_url = "https://data.open-power-system-data.org/when2heat/2023-07-27/when2heat.csv"
        w2h = CSV.read(HTTP.get(w2h_url).body,DataFrame)
        cols = reduce(vcat,collect(Iterators.product(replace(dieter_data.sets["countries"],"UK" => "GB"),["_heat_profile_"],["space","water"],["_"],["SFH","MFH","COM"])))
        f_tmp = x-> string(x...)
        cols = [ f_tmp(x) for x in cols]

     
        select!(w2h,vcat(["cet_cest_timestamp"],cols))
        for col in cols
            w2h[!,col] = convert.(Float64,w2h[!,col])    
        end
            
        w2h[!,:cet_cest_timestamp] = g.(w2h[!,:cet_cest_timestamp])
        filter!(r-> Dates.year(r.cet_cest_timestamp) in [demand_year,demand_year+1] ,w2h) 
        filter!(r -> r.cet_cest_timestamp >= Dates.DateTime(demand_year,7,1) && r.cet_cest_timestamp < Dates.DateTime(demand_year+1,7,1),w2h)

        w2h = stack(w2h,cols,variable_name=:var, value_name=:heat_demand)
        w2h[!,:country] = map(x->x[1:2],w2h[!,:var])
        w2h[!,:type] = map(x->x[(end-2):end],w2h[!,:var])
        w2h[!,:type] = replace.(w2h[!,:type],"SFH" => "Residential","MFH" => "Residential","COM" => "Commercial")
        w2h[!,:sink] = map(x->x[(end-8):(end-4)],w2h[!,:var])
        select!(w2h,Not(:var))
        w2h = combine(groupby(w2h,[:cet_cest_timestamp,:country,:type,:sink]),:heat_demand => mean => :heat_demand)

        jrc = jrc_data_heat(dieter_data,2015; country_set=["UK"])
        jrc2021 = jrc_data_heat(dieter_data,2021)
        jrc2021 = vcat(jrc2021,filter(x->x.country=="UK",jrc))
       
        elec_dem = select(jrc2021,[:country,:type,:var,:fec])
        elec_dem[!,:sink] = map(x->lowercase(x[1:5]),elec_dem[!,:var])
        replace!(w2h[!,:country],"GB" => "UK")
        leftjoin!(w2h,elec_dem,on=[:country,:type,:sink])
        w2h[!,:fec] = w2h[!,:fec]./1e6



        w2h[!,:elec_hd] = w2h[!,:heat_demand] .* w2h[!,:fec]
        w2h = combine(groupby(w2h,[:cet_cest_timestamp,:country]),:elec_hd => sum => :elec_hd)
        w2h[!,:day] = floor.(w2h[!,:cet_cest_timestamp],Dates.Day(1))
        w2h = combine(groupby(w2h,[:day,:country]),:elec_hd => mean => :mean_elec_hd)
       
        demand[!,:day] = floor.(demand[!,:cet_cest_timestamp],Dates.Day(1))
        leftjoin!(demand,w2h,on=[:day,:country])

        demand[!,:corr_demand] = demand[!,:demand] .- demand[!,:mean_elec_hd]
        
    end

    # Add industrial electricity demand
    demand = leftjoin(demand,industry_elec_demand(dieter_data;add_h2_demand=true),on=:country) 
    demand[!,:corr_demand] = demand[!,:corr_demand] + demand[!,:elec_add].*1e6/8760

    # rename country to n
    rename!(demand,:country => :n)

    # Add EV demand
    ev = exogenous_ev_load(dieter_data,flat=true)
    demand = leftjoin(demand,ev,on=[:n,:h])
    demand[!,:corr_demand] = demand[!,:corr_demand] + demand[!,:ev_demand_flat]

    # prepare final electricity demand
    demand[!,:demand] = demand[!,:corr_demand]
    select!(demand,[:n,:h,:demand])
    rename!(demand,:demand=>:load)
    demand = unstack(demand,:n,:load)


    return demand
end

function elec_heat_norway()
    tmp = DataFrame(
        :var => ["Space heating","Water heating","Space heating","Water heating"],
        :tes => [0.8*37.5*1e6,0.2*37.5*1e6,0.8*13.9*1e6,0.2*13.9*1e6],
        :country => ["NO","NO","NO","NO"],
        :type => ["Residential","Residential","Commercial","Commercial"],
        :fec => [0.8*37.5*0.8*1e6/3.5,0.2*37.5*0.8*1e6/3.5,0.8*13.9*0.7*1e6/3.5,0.2*13.9*0.7*1e6/3.5], # Assumption:  https://publikasjoner.nve.no/eksternrapport/2020/eksternrapport2020_08.pdf 80% of residential heating is electric, ≈70% of commercial heating is electric, 80/20 space/water based on other countries, assume an average COP of 3.5
    
        )
 #   tmp[!,:factor] = tmp.tes ./ tmp.fec
    return tmp
end

function elec_heat_switzerland()
    tmp = DataFrame(
        :var => ["Space heating","Water heating","Space heating","Water heating"],
        :tes => [1,1,1,1], # Assumption:https://waermeinitiative.ch/wp-content/uploads/sites/3/2021/08/FESS_WIS_Fokusstudie_Waerme.pdf 1/3 to 2/3 for Residential commerical ,175 PJ / 
        :country => ["CH","CH","CH","CH"],
        :type => ["Residential","Residential","Commercial","Commercial"],
        :fec => [0.66*175/3.6*0.09/2,0.66*32/3.6*0.27/2,0.33*175/3.6*0.09/2,0.33*32/3.6*0.27/2], # Assumption:https://waermeinitiative.ch/wp-content/uploads/sites/3/2021/08/FESS_WIS_Fokusstudie_Waerme.pdf 1/3 to 2/3 for Residential commerical ,175 PJ / 
    )
    return tmp
end

function industry_elec_demand(dieter_data::DieterData;add_h2_demand::Bool=true)
    # Function to add a profile of additional electricity demand due to industry electrification
    #demand = dieter_data.demand_ts
    #countries = dieter_data.sets["countries"]
    proj_year = dieter_data.scalars["proj_year"]
    if !isfile("downloads/spatial-sector-results-v2.zip")
        file = HTTP.get("https://zenodo.org/records/8006612/files/spatial-sector-results-v2.zip?download=1")
        write("downloads/spatial-sector-results-v2.zip",file.body)
    end
    zip = ZipFile.Reader("downloads/spatial-sector-results-v2.zip")
    file = filter(x->x.name =="workflows/pypsa-eur-sec/resources/industrial_energy_demand_elec_s_90_2050.csv",zip.files)[1]
    df = CSV.read(file,DataFrame)
    rename!(df, Symbol(names(df)[1]) => :region)
    df = stack(df,Not(:region),variable_name=:carrier,value_name=:demand)
    df[!,:country] = map(x->x[1:2],df[!,:region])
    df = combine(groupby(df,[:country,:carrier]),:demand => sum => :demand)
    df = unstack(df,:carrier,:demand)
    df[!,:elec_add] = df[!,:electricity] - df[!,Symbol("current electricity")]
    replace!(df[!,:country],"GB" => "UK")
    df[!,:elec_add] = df[!,:elec_add] .* (proj_year - 2015) / (2050 - 2015) # Assume linear interpolation of additional industrial electricity demand
    if add_h2_demand
        dieter_data.h2_demand = select(df,[:country,:hydrogen])
    end
    return select(df,[:country,:elec_add])
end





function jrc_data_heat(dieter_data::DieterData,jrc_year::Int64=2015;country_set::Union{Vector{String},Nothing}=nothing)
    demand_year = min(dieter_data.scalars["demand_year"],jrc_year)
    # download JRC IDEES Data
    if !isnothing(country_set)
        countries = country_set
    else
        countries  = dieter_data.sets["countries"]
    end
    fec_dict = Dict{String,Float64}()
    heat_dict = Dict{String,Float64}()
    conv_factor = 11.63 * 1e3 # MWh per ktoe
    elec_mapping = Dict(
        "Advanced electric heating" => "Space heating",
        "Conventional electric heating" => "Space heating",
        "Electricity in circulation" => "Space heating ",
        "Electricity in circulation and other use" => "Space heating ",
        "Electricity" => "Water heating",
    )

    if jrc_year == 2021
     jrc_link = "https://jeodpp.jrc.ec.europa.eu/ftp/jrc-opendata/JRC-IDEES/JRC-IDEES-2021_v1/JRC-IDEES-2021_"# 2021
    else
     jrc_link = "https://jeodpp.jrc.ec.europa.eu/ftp/jrc-opendata/JRC-IDEES/JRC-IDEES-2015_v1/JRC-IDEES-2015_All_xlsx_" # 2015
    end
    
    country_cont = DataFrame[]
    for c in setdiff(countries,["CH"])
        println(c)
        if c == "GR"
            cc = "EL"
        else 
            cc = c
        end

        if c == "NO" continue end
        if jrc_year == 2021 && c == "UK" continue end

        url = "$(jrc_link)$(cc).zip"
        res = HTTP.get(url).body
        zip = ZipFile.Reader(IOBuffer(res))
        tes_cont = []
        fec_cont = []
        for i in 1:2
            tt = ["Residential","Tertiary"][i]
            idx = findfirst((x) -> occursin(tt, x.name), zip.files)
            file = zip.files[idx]
            ss = ["RES","SER"][i]
            tes,fec =    mktemp() do path, io
                write(io, file)
                DataFrame.(XLSX.readtable.(path, ["$(ss)_hh_tes","$(ss)_hh_fec"]))
            end
            rename!(tes, Symbol(names(tes)[1]) => :var) 
            filter!(x->x.var in ["Space heating","Water heating","Hot water"],tes)
            select!(tes,[:var,Symbol(string(demand_year))])
            rename!(tes, Symbol(string(demand_year)) => :tes)
            
            rename!(fec, Symbol(names(fec)[1]) => :var) 
            filter!(x->x.var in [
            "Advanced electric heating", 
            "Conventional electric heating",
            "Electricity in circulation",
            "Electricity",
            "Electricity in circulation and other use"],fec)
            fec = first(fec,nrow(fec)-1) # Exclude electricity used for cooking/catering 
            select!(fec,[:var,Symbol(string(demand_year))])
            rename!(fec, Symbol(string(demand_year)) => :fec)

            tes[!,:country] .= c
            tes[!,:type] .= tt
            fec[!,:country] .= c
            fec[!,:type] .= tt

            push!(tes_cont,tes)
            push!(fec_cont,fec)
        end
        tes = vcat(tes_cont...)
        fec = vcat(fec_cont...)
        tes[!,:var] = replace.(tes[!,:var],"Hot water" => "Water heating")


        fec[!,:var] = map(x->elec_mapping[x],fec[!,:var]) 
        fec = combine(groupby(fec,[:country,:type,:var]),:fec => sum => :fec)
        df = leftjoin(tes,fec,on=[:country,:type,:var])
        replace!(df[!,:type], "Tertiary" => "Commercial")
        df[!,[:tes,:fec]] = conv_factor.*df[!,[:tes,:fec]]        

        push!(country_cont,df)
        
    end
    df = vcat(country_cont...)
    df = vcat(df,elec_heat_norway(),elec_heat_switzerland())
    df[!,:factor] = df.fec ./ df.tes
    return  df
end


function compile_weather(dieter_data::DieterData)
    # Currently data is based on PECD from ERAA21
    # 
    # TODO: Need to allow for the possibility of using alternative data sources such as the new ERAA23 dataset
    
    monthly_ts = parse(Bool,dieter_data.scalars["monthly_ts"])
    start_year = dieter_data.scalars["start_year"]
    end_year = dieter_data.scalars["end_year"]
    base_year = dieter_data.scalars["weather_year"]

    start_month = dieter_data.scalars["start_month"]
    countries = dieter_data.sets["countries"]

    eraa_year = dieter_data.scalars["eraa_year"]
    eraa_weather_path = dieter_data.paths["eraa_weather_path"]

    # import time series
    cfs = import_capacity_factors(dieter_data)
    ror = import_ror_data(dieter_data)
    rvsr_phs = import_rvsr_phs_data(dieter_data)
    # merge time series
    dfs = [cfs,ror,rvsr_phs]
    df = leftjoin(cfs,ror,on=[:n,:year,:day_count])
    df = leftjoin(df,rvsr_phs,on=[:n,:year,:week])
    for t in ["pv","wind_on","wind_off","ror","rsvr","phs_open"]
        replace!(df[!,t], missing => 0.0)
    end

    # filter out years after 2017
    filter(r->r.year <= 2017,df)

    sort(df,[:n,:year,:day_count])
    select!(df,Not(:ror_capacity,:day_count,:day,:week))
    df[!,:season] = map(row -> get_season(row.month,row.year), eachrow(df))
    df[!,:month_seas] = map(x-> x.month > 6 ? x.month - 6 : x.month + 6,eachrow(df))
    sort!(df,[:n,:season,])
    df[!,:count] = combine(groupby(df,[:n,:season]),eachindex => :count)[!,:count]
    df[!,:h] = string.("t",lpad.(df.count,4,"0"))

    seas = combine(groupby(df,:season),nrow => :count)
    max_hours = maximum(seas[!,:count])
    seas = filter(r->r.count == max_hours,seas)[!,:season] |> Array |> vec

    if monthly_ts
        weather_dict = Dict{String,Dict{String,DataFrame}}()
        for s in seas
            tmp = filter(x->x.season == s,df)
            weather_dict[s] = Dict{String,DataFrame}()
            for m in 1:12
                tmp2 = filter(x->x.month_seas == m,tmp)
                select!(tmp2,[:n,:h,:pv,:wind_on,:wind_off,:ror,:rsvr,:phs_open])
                weather_dict[s][string(m)] = tmp2
            end
        end 
    else
        weather_dict = Dict{String,DataFrame}()
        for s in seas
            tmp =filter(x->x.season==s,df)
            weather_dict[s] = tmp
        end
    end
    return weather_dict

end

function get_season(month,year)
    if month >= 7 
        return "$(round(Int,year))/$(round(Int,year+1))"
    else
        return "$(round(Int,year-1))/$(round(Int,year))"
    end
end


function renumber_months()
end



function import_capacity_factors(dieter_data::DieterData)
    eraa_weather_path = dieter_data.paths["eraa_weather_path"]
    eraa_year = dieter_data.scalars["eraa_year"]
    name_dict = Dict("LFSolarPV" => "pv","Onshore" => "wind_on","Offshore" => "wind_off")
    dfs = []
    for t in ["LFSolarPV","Onshore","Offshore"]
        tmp_path =  replace(joinpath(eraa_weather_path,"PECD-2021.3-country-$(t)-$(eraa_year).parquet"),"\\" => "/")
        tmp_df = @pipe HTTP.get(tmp_path).body |> Parquet2.Dataset |> DataFrame
        rename!(tmp_df, :cf  => name_dict[t])   
        push!(dfs,tmp_df)
    end
    df = outerjoin(dfs...,on=[:country,:year, :month, :day, :hour])
    for t in collect(values(name_dict))
        replace!(df[!,t], missing => 0.0)
    end
    df[!,:day_count] = combine(groupby(df,[:country,:year]),:hour => (x -> 1:length(x)) => :count)[!,:count]./24 .|> ceil
    df[!,:week]  = combine(groupby(df,[:country,:year]),:hour => (x -> 1:length(x)) => :count)[!,:count]./168 .|> ceil
    rename!(df, :country => :n)
    return df
end


function import_ror_data(dieter_data::DieterData)
    eraa_weather_path = dieter_data.paths["eraa_weather_path"]
    eraa_year = dieter_data.scalars["eraa_year"] 
    # import data for run-of-river
    tmp_url = replace(joinpath(eraa_weather_path,"PECD_EERA2021_ROR.zip"),"\\" => "/")
    res = HTTP.get(tmp_url).body
    zip = ZipFile.Reader(IOBuffer(res))
    a_file_in_zip = filter(x->x.name =="PECD_EERA2021_ROR_$(eraa_year)_country_gen.parquet",zip.files)[1]
    tmp_df = @pipe a_file_in_zip |> Parquet2.Dataset |> DataFrame |> sort(_,[:country,:year])
    tmp_df[!,:year] = parse.(Int64,tmp_df[!,:year])
    rename!(tmp_df, :Day => :day_count, :gen_GWh => :ror,:country => :n)
    tmp_capa = select(filter(row -> row.tech == "ror",dieter_data.technologies),[:n,:minimum_capacity,:maximum_capacity])
    @assert sum( 1 .- (tmp_capa.minimum_capacity .== tmp_capa.maximum_capacity)) == 0 "Run-of-river capacity needs to be fixed"
    select!(tmp_capa,[:n,:maximum_capacity])
    rename!(tmp_capa,:maximum_capacity => :ror_capacity)
    tmp_df = leftjoin(tmp_df,tmp_capa,on=:n)
    tmp_df[!,:ror] = tmp_df[!,:ror] ./ tmp_df[!,:ror_capacity] * 1000/24

    return tmp_df
end



function import_rvsr_phs_data(dieter_data::DieterData)
    eraa_weather_path = dieter_data.paths["eraa_weather_path"]
    eraa_year = dieter_data.scalars["eraa_year"] 
    # import data for reservoirs and pumped hydro
    tmp_url = replace(joinpath(eraa_weather_path,"PECD_EERA2021_reservoir_pumping.zip"),"\\" => "/")
    res = HTTP.get(tmp_url).body
    zip = ZipFile.Reader(IOBuffer(res))
    a_file_in_zip = filter(x->x.name =="PECD_EERA2021_reservoir_pumping_$(eraa_year)_country_inflow.parquet",zip.files)[1]
    tmp_df = @pipe a_file_in_zip |> Parquet2.Dataset |> DataFrame |> sort(_,[:country,:year])
    tmp_df[!,:year] = parse.(Int64,tmp_df[!,:year])
    filter!(row->row.technology in ["reservoir","pumped_open"],tmp_df)
    tmp_df[!,:technology] = replace.(tmp_df[!,:technology], "reservoir" => "rsvr","pumped_open" => "phs_open")
    tmp_df[!,:inflow] = tmp_df[!,:inflow_GWh] ./ 168 * 1e3
    replace!(tmp_df[!,:inflow], missing => 0.0)
    select!(tmp_df,Not(:inflow_GWh))
    tmp_df = unstack(tmp_df,[:country,:year,:Week],:technology,:inflow)
    rename!(tmp_df, :Week => :week, :country => :n)
    return tmp_df
end

function compile_heat_data(dieter_data::DieterData)
    heat_flag = parse(Bool,dieter_data.scalars["heat_flag"])
    monthly_ts = parse(Bool,dieter_data.scalars["monthly_ts"])
    heat_data_path = dieter_data.paths["heat_profile_path"]

    start_year = dieter_data.scalars["start_year"]
    end_year = dieter_data.scalars["end_year"]
    base_year = dieter_data.scalars["weather_year"]
    proj_year = dieter_data.scalars["proj_year"]
    heat_profile_path = dieter_data.paths["heat_profile_path"]
    heat_demand_path = dieter_data.paths["heat_demand_path"]
    heat_countries = select(filter(row->row.heat == 1,dieter_data.extensions),:n) |> Array |> vec
    if heat_countries == []
        return @warn("The set of heat countries is empty. Activate heat extension for at least one country.")
    end
    heat_profiles = Dict{Int64,DataFrame}()
    # obtain file names
    s_tmp = x-> chop(x;tail = length(x) -  findfirst.("_",x)[1]+1)
    vars = s_tmp.(readdir(heat_profile_path)) |> unique
    filter!(x-> contains(x,"airSource") || x == "spaceHeat",vars)
    for y in start_year:end_year
        tmp_path = joinpath(heat_data_path,"spaceHeat_$(proj_year)_hour_country_$(y).csv")
        tmp_df = CSV.read(tmp_path,DataFrame)
        rename!(tmp_df, :country => :n)
        filter(x->x.n in heat_countries,tmp_df)
        tmp_df[!,:year] .= y
        rename!(tmp_df,:value => :spaceHeat)
        for v in setdiff(vars,["spaceHeat"])
            tmp_path = joinpath(heat_data_path,"$(v)_$(2030)_hour_country_$(y).csv")
            tmp_df2 = CSV.read(tmp_path,DataFrame)
            rename!(tmp_df2, :country => :n)
            rename!(tmp_df2,:value => v)
            tmp_df = leftjoin(tmp_df,tmp_df2,on=[:n,:hour])
        end
        # add dummy date to get month_seas
        tmp_df[!,:date] = map(x-> Dates.DateTime(2022,1,1) + Dates.Hour.(x - 1),tmp_df[!,:hour])
        tmp_df[!,:month] = Dates.month.(tmp_df[!,:date])
        tmp_df[!,:season] = map(row -> get_season(row.month,row.year), eachrow(tmp_df))
        tmp_df[!,:month_seas] = map(x-> x.month > 6 ? x.month - 6 : x.month + 6,eachrow(tmp_df))
        
        heat_profiles[y] = tmp_df
        
    end
    heat_profiles = reduce(vcat,collect(values(heat_profiles)))

    # normalize space heat to annual demand of 1
    transform!(groupby(heat_profiles,[:n,:year]),:spaceHeat .=> (x -> x ./ sum(x)) => :spaceHeat_norm)

    # Import annual heat demands
    heat_demand = CSV.read(heat_demand_path,DataFrame)
    filter!(x->x.year == proj_year,heat_demand)
    heat_demand = combine(groupby(heat_demand,[:n]),:value => sum => :heat_demand)

    # merge heat profiles with heat demand
    heat_profiles = leftjoin(heat_profiles,heat_demand,on=[:n])
    heat_profiles[!,:heat_demand] = heat_profiles[!,:heat_demand] .* heat_profiles[!,:spaceHeat_norm]
    heat_profiles[!,:heat_demand] = heat_profiles[!,:heat_demand] *1e6

    sort!(heat_profiles,[:n,:season,:month_seas])
    heat_profiles[!,:count] = combine(groupby(heat_profiles,[:n,:season]),eachindex => :count)[!,:count]
    heat_profiles[!,:h] = string.("t",lpad.(heat_profiles.count,4,"0"))
    select!(heat_profiles,vcat([:n,:season,:month_seas,:h,:heat_demand],Symbol.(setdiff(vars,["spaceHeat"]))))

    seas = combine(groupby(heat_profiles,:season),nrow => :count)
    max_hours = maximum(seas[!,:count])
    seas = filter(r->r.count == max_hours,seas)[!,:season] |> Array |> vec

    if monthly_ts
        heat_dict = Dict{String,Dict{String,DataFrame}}()
        for s in seas
            tmp = filter(x->x.season == s,heat_profiles)
            heat_dict[s] = Dict{String,DataFrame}()
            for m in 1:12
                tmp2 = filter(x->x.month_seas == m,tmp)
                heat_dict[s][string(m)] = tmp2
            end
        end 
    else
        heat_dict = Dict{String,DataFrame}()
        for s in seas
            tmp =filter(x->x.season==s,heat_profiles)
            heat_dict[s] = tmp
        end
    end

    return heat_dict

end


function exogenous_ev_load(dieter_data::DieterData;smooth::Bool=true,flat::Bool=false)
    ev_quant = CSV.read(dieter_data.paths["ev_quant_path"],DataFrame)
    ev_ts = CSV.read(dieter_data.paths["ev_ts_path"],DataFrame)
    ev_data = CSV.read(dieter_data.paths["ev_data_path"],DataFrame)

    # prepare time series
    ev_ts = filter(x->x.headers_time_ev == "ev_ged_exog",ev_ts)
    select!(ev_ts,Not(:headers_time_ev))
    ev_ts = stack(ev_ts,Not([:n,:ev]),variable_name=:h,value_name=:ev_ged_exog)

    ev_ts[!,:num] = parse.(Int64,replace.(ev_ts[!,:h],"t" => ""))
    ev_ts[!,:num] = ifelse.(ev_ts[!,:num] .< 4345, ev_ts[!,:num] .+ 4416,ev_ts[!,:num] .- 4344)
    ev_ts[!,:h] = string.("t",lpad.(ev_ts.num,4,"0"))
    select!(ev_ts,Not(:num))

    # add shares
    select!(ev_data,[:n,:ev,:share_ev])
    ev_ts = leftjoin(ev_ts,ev_data,on=[:n,:ev])

    # add quantity
    select!(ev_quant,[:n,:ev_quant])
    ev_ts = leftjoin(ev_ts,ev_quant,on=[:n])
    ev_ts[!,:ev_demand] = ev_ts[!,:ev_ged_exog] .* ev_ts[!,:ev_quant] .* ev_ts[!,:share_ev]
    ev_ts = combine(groupby(ev_ts,[:n,:h]),:ev_demand => sum => :ev_demand)
    ev_ts = transform(groupby(ev_ts,:n),:ev_demand => (x-> RollingFunctions.runmean(x,24)) => :ev_demand_smooth)
    ev_ts = transform(groupby(ev_ts,:n),:ev_demand_smooth => (x-> RollingFunctions.runmean(x,24)) => :ev_demand_smooth_bw)
    sort!(ev_ts,[:n,:h],rev=false)
    select!(ev_ts,Not(:ev_demand_smooth))
    rename!(ev_ts,:ev_demand_smooth_bw => :ev_demand_smooth)
    
    if flat
        tmp = combine(groupby(ev_ts,:n),:ev_demand_smooth => mean => :ev_demand_flat)
        leftjoin!(ev_ts,tmp,on=:n)
    end
    
    sort!(ev_ts,[:n,:h],rev=false)



    return ev_ts
end


