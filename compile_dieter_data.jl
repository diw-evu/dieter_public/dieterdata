# -----------------------------------------------------------------------------
# Import scripts
include("src/julia/struct.jl")
include("src/julia/util.jl")
include("src/julia/generators.jl")
include("src/julia/storages.jl")
include("src/julia/heat.jl")
include("src/julia/time_series.jl")
include("src/julia/grid.jl")
include("src/julia/h2.jl")


# -----------------------------------------------------------------------------
# Define dataset
sets_dict = Dict{String,Any}(
    "countries" => ["DE","FR","NL","AT","BE","CH","DK","UK","IT","NO","ES"],
    "extensions" => Dict("h2" => ["DE","FR","NL","AT","BE","CH","DK","UK","IT","NO","ES"],
            "heat" => ["DE","FR","NL","AT","BE","CH","DK","UK","IT","NO","ES"], ),
    "tech" => ["pv","wind_on","wind_off","ror","bio","nuc"],
    "sto" => ["rsvr","phs_open","phs_closed","Li-Ion"],
    "elyh2" => ["PEM"],   
    "stoh2" => ["cavern","tank storage"],
    "reconh2" => ["h2 gt"],
    "heat_tech" => ["airSourceFloor"],
    #"heat_sink" => ["floor"],
    "heat_building" => ["heat_demand"],
)

scalars_dict = Dict{String,Union{String,Int64,Float64}}(
    "proj_year" => 2050, # target year of system optimization - reflects cost projections for that year
    "weather_year" => 2013, # Weather year to which runs default 
    "currency_year" => 2022, # Costs are in real terms of this year
    
    "ts_resolution" => 4, # Time series resolution in hours
    
    "min_res" => 0., # Minimum renewable share
    "infeas_cost" => 1e5, # Uniform infeasibility costs applied to all energy carriers
    "interest_rate" => 0.04, # Interest rate for discounting


    "eraa_year_sto" => 2030, 
    "upper_bound_gas" => 1e5, # OCGT and CCGT capacity limits in MW
    "ocgt_share" => 1/3, # ERAA only provides capacities for generic gas technology. This allows to split the capacities between OCGT and CCGT
    "eraa_year" => 2030, # ERAA has weather data for different target years
    "start_year" => 1982, # First year of weather data
    "end_year" => 2017, # Last year of weather data
    "start_month" => 7, # Determines the tempoeral structure of the model
    "potential_type" => "technical",
    "demand_year" => 2017, # Year of demand data
    "elec_heat_correction" => 1, # Correct historical electricity demand for electrified heat
    "heat_flag" => "true"   , # Compile heat demand - if not heat demand and cops drawn from repo
    "monthly_ts" => "true", # Compile monthly time series
    "write_data_to_csv" => 1, # Write data to csv

    "heat_share" => 0.8, # Share of heat demand covered by represented system
    "heat_tech" => "airSourceFloor", # Heat pump technology
    "heat_sink" => "Floor", # Heat sink: Radiat"or, floor or water
    "building" => "heat_demand"

)

path_dict = Dict(
    "tech_data" => "manual_data/tech_data.csv",
    "inflation" => "manual_data/prc_hicp_aind_page_linear.csv",
    "fuel_data" => "manual_data/fuel_costs.csv",
    "eraa_weather_path" => "https://zenodo.org/records/7224854/files",
    "eraa_capas_path" => "https://zenodo.org/records/7224854/files/capacities-national-estimates.csv?download=1",
    "res_potentials" => "https://zenodo.org/records/5112963/files/possibility-for-electricity-autarky.zip?download=1",
    "eraa_sto_path" => "https://zenodo.org/records/7224854/files/PECD_EERA2021_reservoir_pumping.zip?download=1",
    "cavern_potential_path" => "manual_data/cavern_potential.csv",
    "heat_profile_path" => "manual_data/heat_profiles/",
    "heat_demand_path" => "manual_data/annual_heat_demand.csv",
    "ev_data_path" => "manual_data/ev_data.csv",
    "ev_ts_path" => "manual_data/ev_time_data_upload.csv",
    "ev_quant_path" => "manual_data/ev_quant.csv",
)


# -----------------------------------------------------------------------------
# Compile dieter_data object
dieter_data = DieterData(sets_dict,scalars_dict,path_dict)

# -----------------------------------------------------------------------------
# Write data to file
write_dieter_data(dieter_data)

