# DieterData
This repository compiles input data for dieterpy and DIETERjl from various sources. Technology data are mostly based on the Danish Energy Agency's Technology Catalogue. Other sources include ENTSO-E and various academic publications.

A python wrapper is in progress.