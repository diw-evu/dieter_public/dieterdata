


function create_heat_tech_simple(dieter_data::DieterData)
    countries = dieter_data.sets["extensions"]["heat"]
    heat_tech = dieter_data.scalars["heat_tech"]

    heat_techs = DataFrame(:n => countries)
    heat_techs[!,:heat_tech] .= heat_tech
    heat_techs[!,:heat_storage_efficiency] .= 0.95
    heat_techs[!,:heat_storage_static_efficiency] .= 0.99
    heat_techs[!,:heat_storage_duration].= 1.5
    
    heat_techs[!,:heat_enabled] .= 1

    return heat_techs

end

function create_heat_shares_simple(dieter_data::DieterData)
    countries = dieter_data.sets["extensions"]["heat"]
    heat_share = dieter_data.scalars["heat_share"]
    heat_tech = dieter_data.scalars["heat_tech"]
    heat_building = dieter_data.scalars["building"]
    heat_shares = DataFrame(:n => countries)
    heat_shares[!,:heat_tech] .= heat_tech
    heat_shares[!,:heat_building] .= heat_building
    heat_shares[!,:value] .= heat_share
    return heat_shares
end